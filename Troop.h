#pragma once
#include <iostream>
#include <string>
#include "point.h"
class troop
{
public:
		troop() {};
		~troop() {};
		troop(bool color);
		void removeTroop();
		virtual std::string* get_all_moves(point start) = 0;
		virtual bool move(point start, point dest) = 0;
private:
	bool _color;//true for white false for black
};