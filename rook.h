#pragma once
#include <iostream>
#include <string>
#include "troop.h"
class rook : troop
{
public:
	virtual std::string* get_all_moves(point start);
	virtual bool move(point start, point dest);
private:
	bool _color;//true for white false for black
};